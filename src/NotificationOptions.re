[@bs.deriving jsConverter]
type dir = [
  | `auto
  | `ltr
  | `rtl
]

type t('data) = {
  dir: dir,
  lang: string,
  badge: string,
  body: string,
  tag: string,
  icon: string,
  image: string,
  data: Js.Nullable.t('data),
  vibrate: list(int),
  renotify: bool,
  requireInteraction: bool,
  actions: list(NotificationAction.t),
  silent: bool
};

let init = (data: Js.Nullable.t('data)): t('data) => {
  dir: `auto,
  lang: "",
  badge: "",
  body: "",
  tag: "",
  icon: "",
  image: "",
  data: data,
  vibrate: [],
  renotify: false,
  requireInteraction: false,
  actions: [],
  silent: false
};
