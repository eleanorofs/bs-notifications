type t;

[@bs.get] external action: t => string = "action";
[@bs.get] external title: t => string = "title";
[@bs.get] external icon: t => string = "icon";