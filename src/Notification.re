type t('data);

[@bs.new]
  external makeWithOptions: (string, NotificationOptions.t('data))
  => t('data)
  = "Notification";

[@bs.new] external makeWithoutOptions: (string) => t('data) = "Notification";


/* static properties */

[@bs.scope "Notification"] [@bs.val]
 external permission: string = "permission";

[@bs.scope "Notification"] [@bs.val]
 external maxActions: int = "maxActions";

/* instance properties */
[@bs.get]
  external actions: t('data) => Js.Nullable.t(list(NotificationAction.t))
  = "actions";


[@bs.get] external badge: t('data) => string = "badge";
[@bs.get] external body: t('data) => string = "body";
[@bs.get] external data: t('data) => Js.Nullable.t('data) = "data";
[@bs.get] external dir: t('data) => NotificationOptions.dir = "dir";
[@bs.get] external lang: t('data) => string = "lang";
[@bs.get] external tag: t('data) => string = "tag";
[@bs.get] external icon: t('data) => string = "icon";
[@bs.get] external image: t('data) => string = "image";
[@bs.get] external renotify: t('data) => bool = "renotify";
[@bs.get] external requireInteraction: t('data)=> bool = "requireInteraction";
[@bs.get] external silent: t('data) => bool = "silent";
/* TODO timestamp. Those are integers, right? */
[@bs.get] external title: t('data) => string = "title";

[@bs.get]
  external vibrate: t('data) => Js.Nullable.t(list(int)) = "vibrate";

/* handlers */

type mouseHandler = Dom.mouseEvent => unit;
[@bs.get] external get_onclick: t('data) => mouseHandler = "onclick";
[@bs.set] external set_onclick: (t('data), mouseHandler) => unit = "onclick";

type handler = Dom.event => unit;
[@bs.get] external get_onclose: t('data) => handler = "onclose";
[@bs.set] external set_onclose: (t('data), handler) => unit = "onclose";

[@bs.get] external get_onerror: t('data) => handler = "onerror";
[@bs.set] external set_onerror: (t('data), handler) => unit = "onerror";

[@bs.get] external get_onshow: t('data) => handler = "onshow";
[@bs.set] external set_onshow: (t('data), handler) => unit = "onshow";

type handler_like('a) = 'a => unit; //TODO this could be more specific
[@bs.send] external addEventListener: (t('data), string, handler_like('a))
  => unit
  = "addEventListener";

/* static methods */
[@bs.scope "Notification"] [@bs.val]
external requestPermission: unit => Js.Promise.t(string)
  = "requestPermission";

/* instance methods */
[@bs.send] external close: t('data) => unit = "close";
