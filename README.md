*Note: This project has been forked to follow the new ReScript branding and
syntax in a new package available under 
[`rescript-notifications`](https://npmjs.com/package/rescript-notifications).
This packackage based on ReasonML syntax will still 
be maintained for the forseeable
future.*

# bs-notifications

This package closely wraps the web
[Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API).
It will not include the Notification API Service Worker Additions. Instead,
it will be a dependency of the forthcoming `bs-service-worker`. This package
has not been exhaustively tested. Please open issues. 

## Installation
`npm install bs-notifications`

## Implemented
- [X] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
- [X] [Notification](https://developer.mozilla.org/en-US/docs/Web/API/Notification)
- [X] [NotificationAction](https://developer.mozilla.org/en-US/docs/Web/API/Notification/actions)
- [X] [NotificationEvent](https://developer.mozilla.org/en-US/docs/Web/API/NotificationEvent)
- [X] [NotificationEventInit](https://developer.mozilla.org/en-US/docs/Web/API/NotificationEvent/NotificationEvent)
- [X] [NotificationOptions](https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification)
- [X] [NotificationPermission](https://developer.mozilla.org/en-US/docs/Web/API/Notification/permission)


## Notes
### 'data
You'll notice these types often have a generic type parameter `'data`. This
represents the `data` property of the NotificationOptions passed into the 
Notification constructor. It can be any structured clonable type, though, not
any type. 

### ExtendableEvent
This interface is technically considered part of the Service Worker API, but 
I've included it here instead to break the circular dependency between 
Service Worker API and Notifications API. In my small universe, 
bs-service-worker will depend on bs-notifications, one way. 


## Examples
Forthcoming, my first next priority will be using this package in 
`bs-service-worker`. I'll post links and snippets here afterwards. 

## Contributing
I'm a beginner. I'd be surprised if all of this were definitely right. 
[@ me](https://twitter.com/webbureaucrat) or send me a PR if you find 
anything you think I
should take a second look at. (Stylistically, though, I'll warn you I'm pretty
dug in, at least for now)

## For further reading
I *strongly* recommend you check out my 
[catch-all documentation](https://webbureaucrat.gitlab.io/posts/issues-and-contribution/)
on my 
projects. It describes how to get in touch with me if you have any questions, 
how to contribute code, coordinated disclosure of security vulnerabilities, 
and more. It will be regularly updated with any information I deem relevant
to my side projects. 

## License

License available in LICENSE.md. It's a close adaptation of the Do No Harm 
license. 

If you are concerned about the level of specificity in the license and would 
like to be issued a license more specific to your use case, please open an 
issue, and one will be provided. 
